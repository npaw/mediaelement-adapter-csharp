﻿using Windows.Media.Streaming.Adaptive;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Youbora.adapter;
using Youbora.log;
using Youbora.util;

namespace Youbora.MediaElementAdapter
{
    public class MediaElementAdapter : Adapter
    {
        #region constructor
        public MediaElementAdapter(object playerref) : base(playerref) { }

        public override void RegisterListeners()
        {
            // Start Buffer Monitor
            Monitorplayhead(true, true);

            // Register events
            ((MediaElement)this.player).MediaOpened += Player_MediaOpened;
            ((MediaElement)this.player).CurrentStateChanged += Player_CurrentStateChanged;
            ((MediaElement)this.player).MediaFailed += Player_MediaFailed;
            ((MediaElement)this.player).MediaEnded += Player_MediaEnded;
        }

        public override void UnregisterListeners()
        {
            // UnRegister events
            if (player != null)
            {
                ((MediaElement)this.player).MediaOpened -= Player_MediaOpened;
                ((MediaElement)this.player).CurrentStateChanged -= Player_CurrentStateChanged;
                ((MediaElement)this.player).MediaFailed -= Player_MediaFailed;
                ((MediaElement)this.player).MediaEnded -= Player_MediaEnded;
            }
        }
        #endregion
        #region eventHandlers
        protected virtual void Player_MediaOpened(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Log.Debug("MediaOpened");
            FireJoin();
        }

        protected virtual void Player_CurrentStateChanged(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Log.Debug("State: " + ((MediaElement)this.player).CurrentState.ToString());
            if (((MediaElement)this.player).CurrentState == MediaElementState.Opening)
            {
                FireStart();
            }
            else if (((MediaElement)this.player).CurrentState == MediaElementState.Paused)
            {
                FirePause();
            }
            else if (((MediaElement)this.player).CurrentState == MediaElementState.Buffering)
            {
                FireBufferBegin();
            }
            else if (((MediaElement)this.player).CurrentState == MediaElementState.Playing)
            {
                if (!flags.isStarted)
                {
                    FireStart();
                    monitor.SkipNextTick();
                }
                FireResume();
                if (!flags.isJoined)
                {
                    FireJoin();
                    monitor.SkipNextTick();
                }
            }
            else if (((MediaElement)this.player).CurrentState == MediaElementState.Stopped)
            {
                FireStop();
            }
        }

        protected virtual void Player_MediaFailed(object sender, Windows.UI.Xaml.ExceptionRoutedEventArgs e)
        {
            FireError(e.ErrorMessage);
            FireStop();
        }

        protected virtual void Player_MediaEnded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            FireStop();
        }

        #endregion
        #region getters
        public override string GetVersion()
        {
            return "6.0.5-MediaElement-adapter-csharp";
        }

        public override double GetPlayhead()
        {
            return ((MediaElement)this.player).Position.TotalSeconds;
        }

        public override double GetPlayrate()
        {
            if (flags.isPaused) return 0;
            double playrate = (double)((MediaElement)this.player).PlaybackRate;
            if (playrate == 0 && !flags.isPaused)
            {
                playrate = 1;
            }
            return playrate;
        }

        public override double? GetDuration()
        {
            return ((MediaElement)this.player).NaturalDuration.TimeSpan.TotalSeconds;
        }

        public override string GetPlayerVersion()
        {
            var p = Windows.ApplicationModel.Package.Current.Id.Version;
            return p.Major + "." + p.Minor + "." + p.Build;
        }

        public override string GetPlayerName()
        {
            return "MediaElement";
        }
        #endregion
    }
}
